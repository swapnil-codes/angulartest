import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Employee} from './Employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private _url = './assets/Data/employee.json';

  constructor(private http: HttpClient) { }

  getEmployees():Observable<Employee[]> {
    return this.http.get<Employee[]>(this._url);
  }

  addEmployee(employee:Employee){
    debugger;
    return this.http.post<Employee>(this._url,employee);
  }

// getEmployees(){
//   return[
//     {
//       "id": 1,
//       "name": "Jhon",
//       "phone": "9999999999"
     
//   },
//   {
//       "id": 2,
//       "name": "Jacob",
//       "phone": "AZ99A99PQ9"
     
//   },
//   {
//       "id": 3,
//       "name": "Ari",
//       "phone": "145458522"
     
//   },

//   {
//     "id": 4,
//     "name": "marri",
//     "phone": "ddsfe454546"
   
// },
// {
//   "id": 5,
//   "name": "Arina",
//   "phone": "67687878878"
 
// }
//   ]
// }

}
