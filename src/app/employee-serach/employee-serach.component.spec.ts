import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeSerachComponent } from './employee-serach.component';

describe('EmployeeSerachComponent', () => {
  let component: EmployeeSerachComponent;
  let fixture: ComponentFixture<EmployeeSerachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeSerachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeSerachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
