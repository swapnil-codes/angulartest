import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-serach',
  templateUrl: './employee-serach.component.html',
  styleUrls: ['./employee-serach.component.css']
})
export class EmployeeSerachComponent implements OnInit {
  public employees = [];


  public headElements = ["ID", "Name", "phone No.", "City", "Address1", "Address2", "Portal code"]
  
  constructor(private _employeeService: EmployeeService) { }

  ngOnInit(): void {
    this._employeeService.getEmployees().
      subscribe(data => this.employees = data);
  }

  checkPhoneNo(phoneNo) {
    return /^\d+$/.test(phoneNo);
  }

  filter(value) {
    this.employees = this.employees.filter(emp => emp.name != value);
    this.employees = this.employees.filter(emp => emp.city != value);
  }

}
