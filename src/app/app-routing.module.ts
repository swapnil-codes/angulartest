import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddemployeeComponent } from './addemployee/addemployee.component';
import { EditemployeeComponent } from './editemployee/editemployee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeSerachComponent } from './employee-serach/employee-serach.component';
import { EmployeeAddEditComponent } from './employee-add-edit/employee-add-edit.component';


const routes: Routes = [
  {path:'emplyoees/add',component:AddemployeeComponent},
  {path:'edit/:id',component:EditemployeeComponent},
  {path:'employeelist',component:EmployeeListComponent},
  {path:'employeesearch',component:EmployeeSerachComponent}, 
  {path:'emmployeeaddedit',component:EmployeeAddEditComponent},

  

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingcomponents=[AddemployeeComponent,EditemployeeComponent, EmployeeListComponent, EmployeeSerachComponent, EmployeeAddEditComponent

]
